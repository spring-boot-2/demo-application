# Microservice Full App

This is the whole collection of all the related microservices which all together build our example-application.

# Prepare

````
mvnw clean install
cd api-gateway/booking-app
npm install
````

# Start Frontend

````
cd api-gateway/booking-app
npm run start_frontend
````